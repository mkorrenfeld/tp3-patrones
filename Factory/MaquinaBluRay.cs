﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    public class MaquinaBluRay : Maquina
    {
        public override Soporte CrearSoporte(int capa)
        {
            if (capa == 1)
            {
                return new SoporteBluRay("Simple");
            }
            else if (capa == 2)
            {
                return new SoporteBluRay("Doble");
            }
            else
            {
                return null;
            }
        }
    }
}
