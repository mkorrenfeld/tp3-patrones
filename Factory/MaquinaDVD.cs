﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    public class MaquinaDVD : Maquina
    {
        public override Soporte CrearSoporte(int capa)
        {
            if (capa == 1)
            {
                return new SoporteDVD("Simple");
            }
            else if (capa == 2)
            {
                return new SoporteDVD("Doble");
            }
            else
            {
                return null;
            }
        }
    }
}
