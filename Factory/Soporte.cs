﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    public abstract class Soporte
    {
        protected string _descripcion;
        protected string _capa;
        protected string _capacidad;
        protected string _valor;

        public void Crear()
        {
            Console.WriteLine(string.Format("Se ha creado un nuevo soporte {0} de capa {1} con capacidad de {2} GB. Precio u$s{3}", _descripcion, _capa, _capacidad, _valor));
        }
    }
}
