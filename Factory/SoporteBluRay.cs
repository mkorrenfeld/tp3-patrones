﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    public class SoporteBluRay : Soporte
    {
        public SoporteBluRay(string capa)
        {
            _capa = capa;
            _descripcion = "Blu Ray";
            if (capa == "Simple")
            {
                _capacidad = "50";
                _valor = "20";
            }
            else if(capa == "Doble")
            {
                _capacidad = "100";
                _valor = "40";
            }
            else
            {
                _capa = "Error operación";
                _descripcion = "Error operación";
                _capacidad = "Error operación";
                _valor = "Error operación";
            }
        }
    }
}
