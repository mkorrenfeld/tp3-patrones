﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    public class SoporteDVD : Soporte
    {
        public SoporteDVD(string capa)
        {
            _capa = capa;
            _descripcion = "DVD";
            if (capa == "Simple")
            {
                _capacidad = "4.7";
                _valor = "5";
            }
            else if (capa == "Doble")
            {
                _capacidad = "8.5";
                _valor = "8";
            }
            else
            {
                _capa = "Error operación";
                _descripcion = "Error operación";
                _capacidad = "Error operación";
                _valor = "Error operación";
            }
        }
    }
}
