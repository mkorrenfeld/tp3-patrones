﻿using Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP3_Patrones_Korrenfeld
{
    class Program
    {
        static void Main(string[] args)
        {

            int tipoSoporte = 0;
            int capa = 0;
            int repeticiones = 0;
            int n = 0;

            Maquina maquina;
            Soporte soporte;

            void SolicitarParametros() 
            { 
            Console.WriteLine("Debe introducir el tipo de soporte a crear (1 - DVD / 2 - BluRay)");
            tipoSoporte = int.Parse(Console.ReadLine());
            Console.WriteLine("Debe introducir el tipo de capa (1 - Simple / 2 - Doble)");
            capa = int.Parse(Console.ReadLine());
            Console.WriteLine("Debe introducir la cantidad a crear");
            repeticiones = int.Parse(Console.ReadLine());
            }

            void Fabricar()
            {
                if (tipoSoporte == 1) 
                {
                    maquina = new MaquinaDVD();

                    if (capa == 1) 
                    {
                        soporte = maquina.CrearSoporte(1); //valor 1 = Simple capa -- valor 2 = Doble capa       
                    }
                    else if (capa == 2)
                    {
                        soporte = maquina.CrearSoporte(2);
                    }
                    else 
                    {
                        throw new Exception("Error en parametros de entrada");
                    }
                }
                else if (tipoSoporte == 2) 
                {
                    maquina = new MaquinaBluRay();

                    if (capa == 1)
                    {
                        soporte = maquina.CrearSoporte(1); //valor 1 = Simple capa -- valor 2 = Doble capa
                    }
                    else if (capa == 2)
                    {
                        soporte = maquina.CrearSoporte(2);
                    }
                    else
                    {
                        throw new Exception("Error en parametros de entrada");
                    }
                }    
                else
                {
                    throw new Exception("Error en parametros de entrada");
                }

                for (n = 0; n < repeticiones; n++)
                {
                    soporte.Crear();
                }

            Console.WriteLine("Se han creado {0} soportes", repeticiones);
                Console.WriteLine("");
            }

            while (1==1)
                {
                    Console.WriteLine("Desea fabricar soportes? S/N");
                    if (Console.ReadLine() == "S")
                    {
                        SolicitarParametros();
                        Fabricar();
                    }
                    else
                    {
                    Console.WriteLine("Adios!");
                    Environment.Exit(0);
                    }
                }
           
            Console.ReadKey();
        }                 
    }
}
